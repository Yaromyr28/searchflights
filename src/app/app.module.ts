import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SearchFlightsComponent} from './components/search-flights/search-flights.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule} from '@angular/material/tabs';
import {
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule, MatDividerModule,
  MatInputModule, MatMenuModule,
  MatNativeDateModule, MatProgressBarModule, MatSelectModule, MatSidenavModule,
  MatSnackBarModule, MatTableModule, MatToolbarModule, MatTooltipModule
} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';
import {MainContentPageComponent} from "./pages/main-content-page.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {OverlayModule} from "@angular/cdk/overlay";
import {FlexLayoutModule} from "@angular/flex-layout";
import {OneWayFlightDescriptionCardComponent} from "./components/one-way-flight-description-card/one-way-flight-description-card";
import {ReturnWayFlightDescriptionCardComponent} from "./components/return-way-flight-description-card/return-way-flight-description-card";

@NgModule({
  declarations: [
    AppComponent,
    SearchFlightsComponent,
    MainContentPageComponent,
    NavbarComponent,
    OneWayFlightDescriptionCardComponent,
    ReturnWayFlightDescriptionCardComponent
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    OverlayModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatTabsModule,
    MatNativeDateModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    ),
    MatCardModule,
    MatDividerModule, MatButtonModule, MatMenuModule,
    MatToolbarModule,
    MatSidenavModule, MatSelectModule, MatTooltipModule,
    MatProgressBarModule,
    MatTableModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatFormFieldModule,
    HttpClientInMemoryWebApiModule
  ],
  providers: [InMemoryDataService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
