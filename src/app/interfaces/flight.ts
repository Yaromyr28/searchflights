export class Flight{

  flightNumber:number;
  carrier: string;
  origin: string;
  departure: string;
  destination: string;
  arrival: string;
  aircraft: string;
  distance: string;
  travelTime: string;
  status: string;

}
