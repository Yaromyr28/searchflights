export class CriteriaForSearching {

  flightNumber: number;
  origin: string;
  destination: string;
  departureDate: Date;
  returnDate: Date;

}
