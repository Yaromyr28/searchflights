import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-one-way-flight-description-card-component',
  templateUrl: './one-way-flight-description-card.html',
  styleUrls: ['./one-way-flight-description-card.css']
})
export class OneWayFlightDescriptionCardComponent implements OnInit {

  @Input() searchedResult: any;

  ngOnInit() {
  }

}
