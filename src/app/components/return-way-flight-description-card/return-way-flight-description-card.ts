import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-return-way-flight-description-card-component',
  templateUrl: './return-way-flight-description-card.html',
  styleUrls: ['./return-way-flight-description-card.css']
})
export class ReturnWayFlightDescriptionCardComponent implements OnInit {

  @Input() entry: any;

  ngOnInit() {
  }

}
