import {Component, OnInit} from '@angular/core';
import {SearchFlightsService} from "./search-flights.service";
import {Subscription} from "rxjs/index";

import {CriteriaForSearching} from "../../interfaces/criteria-for-searching";
import {FlightCityInterface} from "../../interfaces/flight-city-interface";
import {MatSnackBar} from "@angular/material";
import {Flight} from "../../interfaces/flight";
import {InMemoryDataService} from "../../in-memory-data.service";


const DEFAULT_DATE = '2018-01-31';

@Component({
  selector: 'app-search-flights',
  templateUrl: './search-flights.component.html',
  styleUrls: ['./search-flights.component.css']
})
export class SearchFlightsComponent implements OnInit {

  usedDate;
  dateArray;

  criteria: CriteriaForSearching = {
    flightNumber: null,
    origin: '',
    destination: '',
    departureDate: new Date(DEFAULT_DATE),
    returnDate: new Date(DEFAULT_DATE),
  };

  private durationOfSnackBarMessage = 2000;
  searchedResultFlights: Flight[];
  searchedResultFlightsWithReturnWay: Map<Flight, Flight>;
  originsOrDestinationsEnum: FlightCityInterface[];
  subscription: Subscription;

  constructor(private searchFlightsService: SearchFlightsService, private snackBarService: MatSnackBar, private inMemoryDataService: InMemoryDataService) {
  }

  ngOnInit() {
    this.originsOrDestinationsEnum = [
      {value: '', viewValue: 'Empty value'},
      {value: 'IAH', viewValue: 'IAH'},
      {value: 'ORD', viewValue: 'ORD'}
    ];
  }


  clickOnSearchButton() {
    if ((this.criteria.flightNumber) ||
      (this.criteria.origin && this.criteria.destination) && this.criteria.departureDate) {
      this.search(this.criteria);
    } else {
      this.snackBarService.open('Some of fields are not valid for searching. ' +
        'Please try one more time.', '', {duration: this.durationOfSnackBarMessage});
    }
  }

  clickOnSearchWithReturnWayButton() {
    if ((this.criteria.origin && this.criteria.destination) && this.criteria.departureDate && this.criteria.returnDate) {
      if (this.criteria.returnDate > this.criteria.departureDate) {
        this.searchWithReturnWay(this.criteria);
      } else {
        this.snackBarService.open('Date of return can not be less than departure date and the same as departure date.', '', {duration: this.durationOfSnackBarMessage});
      }
    } else {
      this.snackBarService.open('Some of fields are not valid for searching. ' +
        'Please try one more time.', '', {duration: this.durationOfSnackBarMessage});
    }
  }


  private search(criteria: CriteriaForSearching): any {
    this.clearData();
    this.subscription = this.searchFlightsService.getFlights().subscribe(
      (res) => {

        this.searchedResultFlights = res
          .filter(flight => this.checkByFlight(criteria, flight, false))
          .filter(flight => this.checkDates(flight.departure, criteria.departureDate));
        if ( criteria.origin.length === 0 || criteria.destination.length === 0 ) {
          this.snackBarService.open('Some of fields are not valid for searching. Please try one more time.', '', {duration: this.durationOfSnackBarMessage});
        } else {
          if (this.searchedResultFlights.length === 0) {
            this.snackBarService.open('Choose a right date!', '', {duration: this.durationOfSnackBarMessage});
          }
        }
        this.searchedResultFlightsWithReturnWay.clear();
      }, (err) => {
        console.log(err);
        this.snackBarService.open(err.message.error, '', {duration: this.durationOfSnackBarMessage});
        console.log(err);
      }
    );

  }

  private searchWithReturnWay(criteria: CriteriaForSearching): any {
    this.clearData();
    this.subscription = this.searchFlightsService.getFlights().subscribe(
      (res) => {
        const fightsFormHome = res.filter(flight => this.checkByFlight(criteria, flight, false)).filter(flight => this.checkDates(flight.departure, criteria.departureDate));
        const fightsToHome = res.filter(flight => this.checkByFlight(criteria, flight, true)).filter(flight => this.checkDates(flight.departure, criteria.returnDate));
        const mapF = new Map<Flight, Flight>();
        fightsFormHome.map(function (x, i) {
          if (fightsToHome[i] != undefined)
            mapF.set(x, fightsToHome[i])
        });
        this.searchedResultFlightsWithReturnWay = mapF;
        this.searchedResultFlights = [];
      }, (err) => {
        this.snackBarService.open(err.message.error, '', {duration: this.durationOfSnackBarMessage});
        console.log(err);
      }
    );
  }

  private checkDates(flightDate: string, criteria: Date): boolean {
    return new Date(flightDate).toDateString() === criteria.toDateString();
  }

  departureFilter = (d: Date): boolean => {
    this.usedDate = this.inMemoryDataService.createDb();
    this.dateArray = this.usedDate.flights;
    const departureDates: Date[] = [];
    for(let key of this.dateArray){
      departureDates.push(new Date(`${key.departure}`));
    }
    let x = false;
    departureDates.forEach(item => {
      if (item.toDateString() == d.toDateString()) {
        console.debug("found valid day:" + d);
        x = true;
      }
    });
    console.debug("invalid day:" + d);
    return x;
  }

  private checkByFlight(criteria: CriteriaForSearching, flight: Flight, isReverse: boolean): boolean {
    if (!isReverse) {
      return (criteria.flightNumber === flight.flightNumber) || (criteria.origin === flight.origin && criteria.destination === flight.destination);
    } else {
      return (criteria.flightNumber === flight.flightNumber) || criteria.destination === flight.origin && criteria.origin === flight.destination;
    }
  }

  private clearData() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.searchedResultFlights = [];
  }

  changeChosenDestination(value: string) {
    this.criteria.destination = value;
  }

  changeChosenOrigin(value: string) {
    this.criteria.origin = value;
  }
  clearAll() {
    this.criteria.flightNumber = null;
    this.criteria.origin = '';
    this.criteria.destination='';
    this.criteria.departureDate = new Date(DEFAULT_DATE);
    this.criteria.returnDate = new Date(DEFAULT_DATE);
    this.searchedResultFlights= [];
    this.searchedResultFlightsWithReturnWay.clear();
  }

}

