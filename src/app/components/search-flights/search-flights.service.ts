import {Injectable} from "@angular/core";
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Flight} from "../../interfaces/flight";


@Injectable({
  providedIn: 'root'
})
export class SearchFlightsService {

  private flightsUrl = "api/flights";

  constructor(private http: HttpClient) {
  }

  getFlights(): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.flightsUrl);
  }
}
